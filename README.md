# Digital_Forensics_Day-02


Volatility Plugins

https://github.com/volatilityfoundation/volatility/wiki/Command-Reference


운영체제 프로필 종류 확인

volatility imageinfo -f mem.raw


프로세스 목록 확인

volatility -f mem.raw --profile=Win7SP1x64 pslist


프로세스 의존성 트리 형태로 확인

volatility -f mem.raw --profile=Win7SP1x64 pstree


프로세스 목록 확인 (종료된 프로세스 포함)

volatility -f mem.raw --profile=Win7SP1x64 psscan


-----
<샘플 메모리 분석 퀴즈>

1. 공격자가 최근에 프런트데스크 컴퓨터에서 보안 관리자(Gideon) 컴퓨터로 접속해서 비밀번호를 덤프 한 것으로 보인다. Gideons의 비밀번호는 무엇인가?



2. 공격자가 Gideon에 대한 액세스 권한을 얻게 되어 파일을 도용하기 위해 모든 파일들을 암호화하여 압축한 것으로 보인다. 공격자는 어떤 암호를 사용 했는가?



3. 공격자가 만든 rar 파일의 이름은 무엇인가?



4. 공격자가 rar 아카이브에 추가한 파일의 이름은 무엇인가? 



5. 공격자의 일정대로 공격자가 보안 관리자의 PC에 예약된 작업을 만든 것으로 보인다. 스케줄링 작업과 연결된 파일의 이름은 무엇인가?

-----

IOC Editor(OpenIOC - Mandiant)

https://www.fireeye.com/content/dam/fireeye-www/services/freeware/sdl-ioc-editor.zip


-----